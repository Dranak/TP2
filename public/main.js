function getWiki (query)
{
		var xmlhttp=new XMLHttpRequest();
		xmlhttp.responseType ="json";
		xmlhttp.onreadystatechange=function()
		{			
		    if (xmlhttp.readyState==4 && xmlhttp.status==200)
		    {
		    	var jsonResult = xmlhttp.response;

		    	//Efface le contenu de la balise ul qui peut contenir le resultat de la dernière recherche

					var ul = document.getElementById("result"); 
					while(ul.firstChild) 
						{
							ul.removeChild(ul.firstChild);
						}
		    	 	for (var j = 0; j < jsonResult[1].length; j++)  // parcour le résultat en format JSON
		    	 	{
		    	 		var li = document.createElement('li');
	      				var a = document.createElement('a');
						var linkText = document.createTextNode(jsonResult[1][j]);
						a.appendChild(linkText);
						a.title = jsonResult[1][j];
						a.href = jsonResult[3][j];
						li.appendChild(a);
						

						document.getElementById("result").appendChild(li);
    				}
		     

		    // console.log( xmlhttp.responseText);
		    }
		}

		xmlhttp.open("GET","http://localhost:1234/api?action=opensearch&format=json&formatversion=2&search="+query+"&namespace=0&limit=10&sug",true);
		xmlhttp.send();
}